json.extract! librarian, :id, :email, :password, :password_confirmation, :created_at, :updated_at
json.url librarian_url(librarian, format: :json)
