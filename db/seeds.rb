# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times do |a|
    Author.create!(name:Faker::Name.name)
end

10.times do |b|
    Book.create!(
    title:Faker::Book.title,
    author:Author.all.sample)
end

10.times do |c|
    Client.create!(name:Faker::Name.name)
end

10.times do |d|
    Librarian.create(email: Faker::Internet.email, password: Faker::Internet.password(min_length: 6))
end

Librarian.create(email: 'admin@admin.com', password: '111111')

10.times do |e|
    Reservation.create!(
        book:Book.all.sample,
        client: Client.all.sample,
        librarian: Librarian.all.sample,
        active: false
    )
end

10.times do |e|
    Reservation.create!(
        book:Book.all.sample,
        client: Client.all.sample,
        librarian: Librarian.all.sample,
        active: true
    )
end