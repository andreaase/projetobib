Rails.application.routes.draw do
  get 'home/index'

  root 'home#index'

  resources :reservations
  resources :clients
  resources :books
  resources :authors
  resources :librarians
  devise_for :librarians, path: 'qq'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
